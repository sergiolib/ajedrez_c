CFLAGS= -g -Wall -pedantic -std=c99
LFLAGS= -g

all : chess

chess : chess.o main.o
	gcc $(LFLAGS) -o chess chess.o main.o

main.o : defs.h

chess.o : chess.h

clean : 
	rm chess main.o chess.o
