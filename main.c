#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "defs.h"

#include "chess.h"

int main() {
  char in1; // 6 bits de posicion
  char in2; // 6 bits de posicion
  int reset; // señal de reset

  char out = 0; // 3 bits: error, black_wins, white_wins

  reset = 1; // reset empieza en 1 para poder restablecer tablero

  char str[BUF_SIZE]; // buffer de entrada desde teclado de posiciones

  char *board; // string de tablero (es necesario limpiarlo luego)
  
  while ((out & 04) == 0) {  // mientras no haya error
    // bloque always @(pos1 and pos2)
    for(;;){ // Input desde entrada estandar
      printf("Ingrese posicion de pieza actual:\n");
      str[0] = '\0';    
      scanf("%s",str);
      if(process_input(&in1, str) == 0) {
        printf("Entrada 1 no válida.\n");
      }
      else
        break;
    }

    for(;;){ // Input desde entrada estandar
      printf("Ingrese posicion final de pieza (pieza por la cual coronar):\n");
      str[0] = '\0';
      scanf("%s",str);
      if(process_input(&in2, str) == 0 || in1 == in2) {
        printf("Entrada 2 no válida.\n");
      }
      else
        break;
    }

    chess_verify(&out, in1, in2, reset); // Verificar jugada
    if (reset == 1) { // Apagar registro de reset
      reset = 0;
    }

    if (out == PROMO){
      for(;;){
        printf("Ingrese pieza para promocionar (2 caballo, 3 alfil, 4 torre, 5 reina):\n");
        str[0] = '\0';
        scanf("%s",str);
        if(process_input_special(&in1,str) == 0)
          printf("Entrada no válida.\n");
        else
          break;
      }
      promote(&out, in1, in2);
    }

    board = showBoard(); // Generar visualizacion de tablero
    printf("%s",board); // Mostrar en salida estandar tablero actual
    free(board); // Para evitar memory leak
  }
  return 0; // El fin nunca se deberia alcanzar, pero debe ir igual
}

