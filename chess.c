#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "defs.h"

char mem[64]; // Arreglo de piezas en el tablero
char curr_player;

char *showBoard() {
  char *str = malloc(1000*sizeof(char));
  int i;
  char *(piece[64]);
  for (i = 0; i<64; i++) {
    switch((mem[i] & 0x0e) >> 1) {
    case 0:
      piece[i] = " ";
      break;
    case 1:
      piece[i] = "P";
      break;
    case 2:
      piece[i] = "C";
      break;
    case 3:
      piece[i] = "A";
      break;
    case 4:
      piece[i] = "T";
      break;
    case 5:
      piece[i] = "D";
      break;
    case 6:
      piece[i] = "R";
      break;
    case 7:
      piece[i] = "R";
      break;
    }
  }
  sprintf(str,"+---+---+---+---+---+---+---+---+\n| %s ||%s|| %s ||%s|| %s ||%s|| %s ||%s||\n+---+---+---+---+---+---+---+---+\n||%s|| %s ||%s|| %s ||%s|| %s ||%s|| %s |\n+---+---+---+---+---+---+---+---+\n| %s ||%s|| %s ||%s|| %s ||%s|| %s ||%s||\n+---+---+---+---+---+---+---+---+\n||%s|| %s ||%s|| %s ||%s|| %s ||%s|| %s |\n+---+---+---+---+---+---+---+---+\n| %s ||%s|| %s ||%s|| %s ||%s|| %s ||%s||\n+---+---+---+---+---+---+---+---+\n||%s|| %s ||%s|| %s ||%s|| %s ||%s|| %s |\n+---+---+---+---+---+---+---+---+\n| %s ||%s|| %s ||%s|| %s ||%s|| %s ||%s||\n+---+---+---+---+---+---+---+---+\n||%s|| %s ||%s|| %s ||%s|| %s ||%s|| %s |\n+---+---+---+---+---+---+---+---+\n",piece[7],piece[15],piece[23],piece[31],piece[39],piece[47],piece[55],piece[63],piece[6],piece[14],piece[22],piece[30],piece[38],piece[46],piece[54],piece[62],piece[5],piece[13],piece[21],piece[29],piece[37],piece[45],piece[53],piece[61],piece[4],piece[12],piece[20],piece[28],piece[36],piece[44],piece[52],piece[60],piece[3],piece[11],piece[19],piece[27],piece[35],piece[43],piece[51],piece[59],piece[2],piece[10],piece[18],piece[26],piece[34],piece[42],piece[50],piece[58],piece[1],piece[9],piece[17],piece[25],piece[33],piece[41],piece[49],piece[57],piece[0],piece[8],piece[16],piece[24],piece[32],piece[40],piece[48],piece[56]);
  return str;
}

void check_board(int reset) {
  if (reset == 1) { 
    int i;
    int j;
    
    curr_player = 1; // Empienzan las blancas
    
    // Indices corresponden a notacion [A-H][1-8]
    
    // Blancas
    mem[0] = 24;
    mem[8] = 20;
    mem[16] = 22;
    mem[24] = 26;
    mem[32] = 28;
    mem[40] = 22;
    mem[48] = 20;
    mem[56] = 24;
    
    for (i = 1; i<58; i+=8)
      mem[i] = 18;
    
    // Negras
    for (i = 6; i<63; i+=8)
      mem[i] = 2;
    
    mem[7] = 8;
    mem[15] = 4;
    mem[23] = 6;
    mem[31] = 10;
    mem[39] = 12;
    mem[47] = 6;
    mem[55] = 4;
    mem[63] = 8;
    
    // Espacios vacios
    for (i = 0; i<8; i++)
      for (j = 0; j<4; j++)
  mem[2+i*8+j] = 0;
  }
}

void check_origin(char *out, char in) {
  int i = (int)in;
  char type_orig = mem[i] & 0x0e;
  type_orig>>=1;
  if (type_orig == 0) {
    *out = ERROR;
  }
}

int in_line(char in, int line) {
  // Verifica que la entrada este en la fila "line"
  if (line >= 0 && line <= 7 && in >= 0 && in <= 63)
    return (in & 07) == line;
  return 0;
}


void is_check(char *out) {
  /***************************************************************************** 
     
     ABSTRACT:    Verifica si los reyes estan en jaque
     DESCRIPTION: Para comprobar que un rey esté en jaque se necesita
     comprobar que no hayan peones, alfiles, caballos, torres o damas
     atacándolo.

  *****************************************************************************/

  int rn = -1;
  int rb = -1;

  int i;  
  // Buscamos los reyes
  for (i = 0; i<64; i++) {
    if (((mem[i] & 0x0e) == 0x0e) || ((mem[i] & 0x0e) == 0x0a)) {
      if ((mem[i] & 0x10) == 0x10) {
  rb = i;
      }
      else {
  rn = i;
      }
    }
  }
  
  int new_pos;
  
  int pos_peon_blancos[2] = {rb+7, rb-9};
  int pos_peon_negros[2] = {rn-7, rn+9};
  int pos_alfil_blancos_d1[9] = {rb+7, rb+14, rb+21, rb+28, rb+35, rb+42, rb+49, rb+56, rb+63};
  int pos_alfil_blancos_d2[9] = {rb-7, rb-14, rb-21, rb-28, rb-35, rb-42, rb-49, rb-56, rb-63};
  int pos_alfil_blancos_d3[7] = {rb+9, rb+18, rb+27, rb+36, rb+45, rb+54, rb+63};
  int pos_alfil_blancos_d4[7] = {rb-9, rb-18, rb-27, rb-36, rb-45, rb-54, rb-63};
  int pos_alfil_negros_d1[9] = {rn+7, rn+14, rn+21, rn+28, rn+35, rn+42, rn+49, rn+56, rn+63};
  int pos_alfil_negros_d2[9] = {rn-7, rn-14, rn-21, rn-28, rn-35, rn-42, rn-49, rn-56, rn-63};
  int pos_alfil_negros_d3[7] = {rn+9, rn+18, rn+27, rn+36, rn+45, rn+54, rn+63};
  int pos_alfil_negros_d4[7] = {rn-9, rn-18, rn-27, rn-36, rn-45, rn-54, rn-63};
  int pos_cab_blancos_d1[4] = {rb-15, rb-6, rb+10, rb+17};
  int pos_cab_blancos_d2[4] = {rb-17, rb-10, rb+6, rb+15};
  int pos_cab_negros_d1[4] = {rn-15, rn-6, rn+10, rn+17};
  int pos_cab_negros_d2[4] = {rn-17, rn-10, rn+6, rn+15};
  int pos_torre_blancos_d1[8] = {rb+1,rb+2,rb+3,rb+4,rb+5,rb+6,rb+7};
  int pos_torre_blancos_d2[8] = {rb-1,rb-2,rb-3,rb-4,rb-5,rb-6,rb-7};
  int pos_torre_blancos_d3[8] = {rb+8,rb+16,rb+24,rb+32,rb+40,rb+48,rb+56};
  int pos_torre_blancos_d4[8] = {rb-8,rb-16,rb-24,rb-32,rb-40,rb-48,rb-56};
  int pos_torre_negros_d1[8] = {rn+1,rn+2,rn+3,rn+4,rn+5,rn+6,rn+7};
  int pos_torre_negros_d2[8] = {rn-1,rn-2,rn-3,rn-4,rn-5,rn-6,rn-7};
  int pos_torre_negros_d3[8] = {rn+8,rn+16,rn+24,rn+32,rn+40,rn+48,rn+56};
  int pos_torre_negros_d4[8] = {rn-8,rn-16,rn-24,rn-32,rn-40,rn-48,rn-56};

  // Chequeo peones negros
  for (i = 0; i<2; i++) {
    new_pos = pos_peon_blancos[i];
    if (new_pos < 64 && new_pos > 0 && ((mem[new_pos] & 0x1e) == 0x02)) {
      mem[rb] = mem[rb] | 0x0e; 
    }
  }

  // Chequeo peones blancos
  for (i = 0; i<2; i++) {
    new_pos = pos_peon_negros[i];
    if (new_pos < 64 && new_pos > 0 && ((mem[new_pos] & 0x1e) == 0x12)) {
      mem[rn] = mem[rn] | 0x0e; 
    }
  }

  // Chequeo alfiles negros
  for (i = 0; i<9; i++) {
    new_pos = pos_alfil_blancos_d1[i];
    if (new_pos > 64 || new_pos < 0 || ((mem[new_pos] & 0x0e) != 0) || in_line(new_pos, 7))
      break; // Si se topó con una pieza, o se esta fuera de rango, o se salto una columna, break
    if (((mem[new_pos] & 0x1e) == 0x06)) {
      mem[rb] = mem[rb] | 0x0e; 
    }
  }

  for (i = 0; i<9; i++) {
    new_pos = pos_alfil_blancos_d2[i];
    if (new_pos > 64 || new_pos < 0 || ((mem[new_pos] & 0x0e) != 0) || in_line(new_pos, 0))
      break; // Si se topó con una pieza, o se esta fuera de rango, o se salto una columna, break
    if (((mem[new_pos] & 0x1e) == 0x06)) {
      mem[rb] = mem[rb] | 0x0e; 
    }
  }

  for (i = 0; i<7; i++) {
    new_pos = pos_alfil_blancos_d3[i];
    if (new_pos > 64 || new_pos < 0 || ((mem[new_pos] & 0x0e) != 0) || in_line(new_pos, 0))
      break; // Si se topó con una pieza, o se esta fuera de rango, o se salto una columna, break
    if (((mem[new_pos] & 0x1e) == 0x06)) {
      mem[rb] = mem[rb] | 0x0e; 
    }
  }

  for (i = 0; i<7; i++) {
    new_pos = pos_alfil_blancos_d4[i];
    if (new_pos > 64 || new_pos < 0 || ((mem[new_pos] & 0x0e) != 0) || in_line(new_pos, 7))
      break; // Si se topó con una pieza, o se esta fuera de rango, o se salto una columna, break
    if (((mem[new_pos] & 0x1e) == 0x06)) {
      mem[rb] = mem[rb] | 0x0e; 
    }
  }

  // Chequeo alfiles blancos
  for (i = 0; i<9; i++) {
    new_pos = pos_alfil_negros_d1[i];
    if (new_pos > 64 || new_pos < 0 || ((mem[new_pos] & 0x0e) != 0) || in_line(new_pos, 7))
      break; // Si se topó con una pieza, o se esta fuera de rango, o se salto una columna, break
    if (((mem[new_pos] & 0x1e) == 0x16)) {
      mem[rn] = mem[rn] | 0x0e; 
    }
  }

  for (i = 0; i<9; i++) {
    new_pos = pos_alfil_negros_d2[i];
    if (new_pos > 64 || new_pos < 0 || ((mem[new_pos] & 0x0e) != 0) || in_line(new_pos, 0))
      break; // Si se topó con una pieza, o se esta fuera de rango, o se salto una columna, break
    if (((mem[new_pos] & 0x1e) == 0x16)) {
      mem[rn] = mem[rn] | 0x0e; 
    }
  }

  for (i = 0; i<7; i++) {
    new_pos = pos_alfil_negros_d3[i];
    if (new_pos > 64 || new_pos < 0 || ((mem[new_pos] & 0x0e) != 0) || in_line(new_pos, 0))
      break; // Si se topó con una pieza, o se esta fuera de rango, o se salto una columna, break
    if (((mem[new_pos] & 0x1e) == 0x16)) {
      mem[rn] = mem[rn] | 0x0e; 
    }
  }

  for (i = 0; i<7; i++) {
    new_pos = pos_alfil_negros_d4[i];
    if (new_pos > 64 || new_pos < 0 || ((mem[new_pos] & 0x0e) != 0) || in_line(new_pos, 7))
      break; // Si se topó con una pieza, o se esta fuera de rango, o se salto una columna, break
    if (((mem[new_pos] & 0x1e) == 0x16)) {
      mem[rn] = mem[rn] | 0x0e; 
    }
  }

  // Chequeo caballos negros
  for (i = 0; i<4; i++) {
    new_pos = pos_cab_blancos_d1[i];
    if (new_pos > 64 || new_pos < 0 || in_line(new_pos,0) == 1 || in_line(new_pos,1) == 1)
      break;
    if (((mem[new_pos] & 0x1e) == 0x07)) {
      mem[rb] = mem[rb] | 0x0e; 
    }
  }

  for (i = 0; i<4; i++) {
    new_pos = pos_cab_blancos_d2[i];
    if (new_pos > 64 || new_pos < 0 || in_line(new_pos,7) == 1 || in_line(new_pos,6) == 1)
      break;
    if (((mem[new_pos] & 0x1e) == 0x07)) {
      mem[rb] = mem[rb] | 0x0e; 
    }
  }

  // Chequeo caballos blancos
  for (i = 0; i<4; i++) {
    new_pos = pos_cab_negros_d1[i];
    if (new_pos > 64 || new_pos < 0 || in_line(new_pos,0) == 1 || in_line(new_pos,1) == 1)
      break;
    if (((mem[new_pos] & 0x1e) == 0x07)) {
      mem[rb] = mem[rb] | 0x0e; 
    }
  }

  for (i = 0; i<4; i++) {
    new_pos = pos_cab_negros_d2[i];
    if (new_pos > 64 || new_pos < 0 || in_line(new_pos,7) == 1 || in_line(new_pos,6) == 1)
      break;
    if (((mem[new_pos] & 0x1e) == 0x07)) {
      mem[rb] = mem[rb] | 0x0e; 
    }
  }

  // Chequeo torres negras
  for (i = 0; i<8; i++) {
    new_pos = pos_torre_blancos_d1[i];
    if (new_pos > 64 || new_pos < 0 || in_line(new_pos,0) == 1 || (mem[new_pos] & 0x0e) != 0)
      break;
    if ((mem[new_pos] & 0x1e) == 0x08) {
      mem[rb] = mem[rb] | 0x0e; 
    }
  }
  for (i = 0; i<8; i++) {
    new_pos = pos_torre_blancos_d2[i];
    if (new_pos > 64 || new_pos < 0 || in_line(new_pos,7) == 1 || (mem[new_pos] & 0x0e) != 0)
      break;
    if ((mem[new_pos] & 0x1e) == 0x08) {
      mem[rb] = mem[rb] | 0x0e; 
    }
  }
  for (i = 0; i<8; i++) {
    new_pos = pos_torre_blancos_d3[i];
    if (new_pos > 64 || new_pos < 0 || (mem[new_pos] & 0x0e) != 0)
      break;
    if ((mem[new_pos] & 0x1e) == 0x08) {
      mem[rb] = mem[rb] | 0x0e; 
    }
  }
  for (i = 0; i<8; i++) {
    new_pos = pos_torre_blancos_d4[i];
    if (new_pos > 64 || new_pos < 0 || (mem[new_pos] & 0x0e) != 0)
      break;
    if ((mem[new_pos] & 0x1e) == 0x08) {
      mem[rb] = mem[rb] | 0x0e; 
    }
  }

  // Chequeo torres blancas
  for (i = 0; i<8; i++) {
    new_pos = pos_torre_negros_d1[i];
    if (new_pos > 64 || new_pos < 0 || in_line(new_pos,0) == 1 || (mem[new_pos] & 0x0e) != 0)
      break;
    if ((mem[new_pos] & 0x1e) == 0x18) {
      mem[rn] = mem[rn] | 0x0e; 
    }
  }
  for (i = 0; i<8; i++) {
    new_pos = pos_torre_negros_d2[i];
    if (new_pos > 64 || new_pos < 0 || in_line(new_pos,7) == 1 || (mem[new_pos] & 0x0e) != 0)
      break;
    if ((mem[new_pos] & 0x1e) == 0x18) {
      mem[rn] = mem[rn] | 0x0e; 
    }
  }
  for (i = 0; i<8; i++) {
    new_pos = pos_torre_negros_d3[i];
    if (new_pos > 64 || new_pos < 0 || (mem[new_pos] & 0x0e) != 0)
      break;
    if ((mem[new_pos] & 0x1e) == 0x18) {
      mem[rn] = mem[rn] | 0x0e; 
    }
  }
  for (i = 0; i<8; i++) {
    new_pos = pos_torre_negros_d4[i];
    if (new_pos > 64 || new_pos < 0 || (mem[new_pos] & 0x0e) != 0)
      break;
    if ((mem[new_pos] & 0x1e) == 0x18) {
      mem[rn] = mem[rn] | 0x0e; 
    }
  }

  // Chequeo dama negra
  for (i = 0; i<9; i++) {
    new_pos = pos_alfil_blancos_d1[i];
    if (new_pos > 64 || new_pos < 0 || ((mem[new_pos] & 0x0e) != 0) || in_line(new_pos, 7))
      break; // Si se topó con una pieza, o se esta fuera de rango, o se salto una columna, break
    if (((mem[new_pos] & 0x1e) == 0x0a)) {
      mem[rb] = mem[rb] | 0x0e; 
    }
  }

  for (i = 0; i<9; i++) {
    new_pos = pos_alfil_blancos_d2[i];
    if (new_pos > 64 || new_pos < 0 || ((mem[new_pos] & 0x0e) != 0) || in_line(new_pos, 0))
      break; // Si se topó con una pieza, o se esta fuera de rango, o se salto una columna, break
    if (((mem[new_pos] & 0x1e) == 0x0a)) {
      mem[rb] = mem[rb] | 0x0e; 
    }
  }

  for (i = 0; i<7; i++) {
    new_pos = pos_alfil_blancos_d3[i];
    if (new_pos > 64 || new_pos < 0 || ((mem[new_pos] & 0x0e) != 0) || in_line(new_pos, 0))
      break; // Si se topó con una pieza, o se esta fuera de rango, o se salto una columna, break
    if (((mem[new_pos] & 0x1e) == 0x0a)) {
      mem[rb] = mem[rb] | 0x0e; 
    }
  }

  for (i = 0; i<7; i++) {
    new_pos = pos_alfil_blancos_d4[i];
    if (new_pos > 64 || new_pos < 0 || ((mem[new_pos] & 0x0e) != 0) || in_line(new_pos, 7))
      break; // Si se topó con una pieza, o se esta fuera de rango, o se salto una columna, break
    if (((mem[new_pos] & 0x1e) == 0x0a)) {
      mem[rb] = mem[rb] | 0x0e; 
    }
  }
  for (i = 0; i<8; i++) {
    new_pos = pos_torre_blancos_d1[i];
    if (new_pos > 64 || new_pos < 0 || in_line(new_pos,0) == 1 || (mem[new_pos] & 0x0e) != 0)
      break;
    if ((mem[new_pos] & 0x1e) == 0x0a) {
      mem[rb] = mem[rb] | 0x0e; 
    }
  }
  for (i = 0; i<8; i++) {
    new_pos = pos_torre_blancos_d2[i];
    if (new_pos > 64 || new_pos < 0 || in_line(new_pos,7) == 1 || (mem[new_pos] & 0x0e) != 0)
      break;
    if ((mem[new_pos] & 0x1e) == 0x0a) {
      mem[rb] = mem[rb] | 0x0e; 
    }
  }
  for (i = 0; i<8; i++) {
    new_pos = pos_torre_blancos_d3[i];
    if (new_pos > 64 || new_pos < 0 || (mem[new_pos] & 0x0e) != 0)
      break;
    if ((mem[new_pos] & 0x1e) == 0x0a) {
      mem[rb] = mem[rb] | 0x0e; 
    }
  }
  for (i = 0; i<8; i++) {
    new_pos = pos_torre_blancos_d4[i];
    if (new_pos > 64 || new_pos < 0 || (mem[new_pos] & 0x0e) != 0)
      break;
    if ((mem[new_pos] & 0x1e) == 0x0a) {
      mem[rb] = mem[rb] | 0x0e; 
    }
  }
  for (i = 0; i<9; i++) {
    new_pos = pos_alfil_negros_d1[i];
    if (new_pos > 64 || new_pos < 0 || ((mem[new_pos] & 0x0e) != 0) || in_line(new_pos, 7))
      break; // Si se topó con una pieza, o se esta fuera de rango, o se salto una columna, break
    if (((mem[new_pos] & 0x1e) == 0x0a)) {
      mem[rb] = mem[rb] | 0x0e; 
    }
  }

  for (i = 0; i<9; i++) {
    new_pos = pos_alfil_negros_d2[i];
    if (new_pos > 64 || new_pos < 0 || ((mem[new_pos] & 0x0e) != 0) || in_line(new_pos, 0))
      break; // Si se topó con una pieza, o se esta fuera de rango, o se salto una columna, break
    if (((mem[new_pos] & 0x1e) == 0x0a)) {
      mem[rb] = mem[rb] | 0x0e; 
    }
  }

  for (i = 0; i<7; i++) {
    new_pos = pos_alfil_negros_d3[i];
    if (new_pos > 64 || new_pos < 0 || ((mem[new_pos] & 0x0e) != 0) || in_line(new_pos, 0))
      break; // Si se topó con una pieza, o se esta fuera de rango, o se salto una columna, break
    if (((mem[new_pos] & 0x1e) == 0x0a)) {
      mem[rb] = mem[rb] | 0x0e; 
    }
  }

  for (i = 0; i<7; i++) {
    new_pos = pos_alfil_negros_d4[i];
    if (new_pos > 64 || new_pos < 0 || ((mem[new_pos] & 0x0e) != 0) || in_line(new_pos, 7))
      break; // Si se topó con una pieza, o se esta fuera de rango, o se salto una columna, break
    if (((mem[new_pos] & 0x1e) == 0x0a)) {
      mem[rb] = mem[rb] | 0x0e; 
    }
  }
  for (i = 0; i<8; i++) {
    new_pos = pos_torre_negros_d1[i];
    if (new_pos > 64 || new_pos < 0 || in_line(new_pos,0) == 1 || (mem[new_pos] & 0x0e) != 0)
      break;
    if ((mem[new_pos] & 0x1e) == 0x0a) {
      mem[rb] = mem[rb] | 0x0e; 
    }
  }
  for (i = 0; i<8; i++) {
    new_pos = pos_torre_negros_d2[i];
    if (new_pos > 64 || new_pos < 0 || in_line(new_pos,7) == 1 || (mem[new_pos] & 0x0e) != 0)
      break;
    if ((mem[new_pos] & 0x1e) == 0x0a) {
      mem[rb] = mem[rb] | 0x0e; 
    }
  }
  for (i = 0; i<8; i++) {
    new_pos = pos_torre_negros_d3[i];
    if (new_pos > 64 || new_pos < 0 || (mem[new_pos] & 0x0e) != 0)
      break;
    if ((mem[new_pos] & 0x1e) == 0x0a) {
      mem[rb] = mem[rb] | 0x0e; 
    }
  }
  for (i = 0; i<8; i++) {
    new_pos = pos_torre_negros_d4[i];
    if (new_pos > 64 || new_pos < 0 || (mem[new_pos] & 0x0e) != 0)
      break;
    if ((mem[new_pos] & 0x1e) == 0x0a) {
      mem[rb] = mem[rb] | 0x0e; 
    }
  }
}

void is_check_mate(char *out) {
  /* ABSTRACT:    Verifica si los reyes estan en jaque mate
     DESCRIPTION: Un rey en jaque mate debe estar en jaque.
                  Si se está en jaque y el rey no se puede mover a ningún lado,
      entonces es un mate. */

  int rn = -1;
  int rb = -1;
  int i;

  // Encontramos los reyes
  for (i = 0; i<64; i++) {
    if ((mem[i] & 0x0e) == 0x0e) {
      if ((mem[i] & 0x10) == 0x10) {
  rb = i;
      }
      else {
  rn = i;
      }
    }
  }

  char aux;
  int new_pos;
  
  int pos_blancas[8] = {rb+1, rb+9, rb+8, rb+7, rb-1, rb-9, rb-8, rb-7};
  int pos_negras[8] = {rn+1, rn+9, rn+8, rn+7, rn-1, rn-9, rn-8, rn-7};
  int b, n;

  for (i = 0; i<8; i++) {
    // Chequeo blancas
    new_pos = pos_blancas[i];
    if (new_pos < 64 && ((mem[new_pos] & 0x0e) == 0)) {
      aux = mem[rb];
      mem[rb] = 0;
      mem[new_pos] = 0x1c;
      is_check(out);
      if ((mem[new_pos] & 0x0e) != 0x0e) {
  mem[rb] = aux;
  mem[new_pos] = 0;
  b = 0;
      }
    }
    // Chequeo negras
    new_pos = pos_negras[i];
    if (new_pos < 64 && ((mem[new_pos] & 0x0e) == 0)) {
      aux = mem[rb];
      mem[rb] = 0;
      mem[new_pos] = 0x1c;
      is_check(out);
      if ((mem[new_pos] & 0x0e) != 0x0e) {
  mem[rb] = aux;
  mem[new_pos] = 0;
  n = 0;
      }
    }
  }

  if (b == 1) {
    *out = BWINS; // Gana negro
  }
  else if (n == 1) {
    *out = WWINS; // Gana blanco
  }
}

void verify_pawn(char *out, char in1, char in2) {
  // Indices
  int i1 = (int)in1;
  int i2 = (int)in2;

  // Color
  char c1 = (mem[i1] & 0x10)>>4;

  // Tipo
  char t2 = (mem[i2] & 0x0e)>>1;

  char good = 0;

  if (((*out & 4) >> 2) == 0) { // si no hay error
    // Verificacion movimiento sin comer
    if (t2 == 0 && c1 == 1 && in_line(in1, 1) && in_line(in2, 3)) {
      // Acciones de movimiento de 2 espacios blancas
      mem[i1] = 0;
      mem[i2] = 0x13; // 1-001-1
      good = 1;
    }
    if (t2 == 0 && c1 == 0 && in_line(in1, 6) && in_line(in2, 4)) {
      // Acciones fila de movimiento de 2 espacios negras
      mem[i1] = 0;
      mem[i2] = 0x03; // 0-001-1
      good = 1;
    }
    if (t2 == 0 && c1 == 1 && (i2-i1 == 1)) {
      // Acciones avance de 1 espacio blancas
      mem[i1] = 0;
      mem[i2] = 0x13; // 1-001-1
      good = 1;
    }
    if (t2 == 0 && c1 == 0 && (i1-i2 == 1)) {
      // Acciones avance de 1 espacio negras
      mem[i1] = 0;
      mem[i2] = 0x03; // 0-001-1
      good = 1;
    }    
    
    // Verificacion movimiento con comer
    
    if (t2 != 0 && c1 == 1 && ((in1+9 == in2) || (in1-7 == in2))) {
      // Accion para comer de las blancas
      mem[i1] = 0;
      mem[i2] = 0x13; // 1-001-1
      good = 1;
    }
    if (t2 != 0 && c1 == 0 && ((in1-9 == in2) || (in1+7 == in2))) {
      // Accion para comer de las negras
      mem[i1] = 0;
      mem[i2] = 0x03; // 0-001-1
      good = 1;
    }

    is_check(out);

    // Verificacion coronacion
    if (good == 1 && ((c1 == 1 && in_line(in1,6)) || (c1 == 0 && in_line(in1,1)))) {
      *out = PROMO;
    }

    // Si no se ejecuto nada de lo anterior, error
    if (good == 0) {
      *out = ERROR;
    }

  }
}

void verify_knight(char *out, char in1, char in2) {
  /****************************************************************************

    ABSTRACT: Verifica movimiento del caballo.
    DESCRIPTION: La pieza en in1 es un caballo, así que hay que
    verificar que el movimiento hacia in2 sea válido.

   *****************************************************************************/
  // Indices
  int i1 = (int)in1;
  int i2 = (int)in2;

  // Color
  char c1 = (mem[i1] & 0x10)>>4;
  char c2 = 0;
  
  // Tipo
  char t2 = (mem[i2] & 0x0e)>>1;

  if (t2 != 0)
    c2 = (mem[i2] & 0x10)>>4;

  char good = 0;
  int i;

  if (((*out & 4) >> 2) == 0) { // si no hay error
    int pos_caballo_arriba_arriba[2] = {i1 - 6, i1 + 10};
    int pos_caballo_arriba[2] = {i1 - 15, i1 + 17};
    int pos_caballo_abajo[2] = {i1 - 17, i1 + 15};
    int pos_caballo_abajo_abajo[2] = {i1 - 10, i1 + 6};

    if ((t2 == 0) || (t2 != 0 && c2 != c1)) { // si la pieza de destino no es del mismo color o no hay pieza
      for (i=0; i<2; i++) {

  if (
      (((in_line(i1, 6) == 0 || in_line(i1, 7) == 0)) && (pos_caballo_arriba_arriba[i] <= 63 && pos_caballo_arriba_arriba[i] >= 0) && (pos_caballo_arriba_arriba[i] == i2))
      || ((in_line(i1, 7) == 0) && (pos_caballo_arriba[i] <= 63 && pos_caballo_arriba[i] >= 0) && (pos_caballo_arriba[i] == i2))
      || ((in_line(i1, 0) == 0) && (pos_caballo_abajo[i] <= 63 && pos_caballo_abajo[i] >= 0) && (pos_caballo_abajo[i] == i2))
      || (((in_line(i1, 1) == 0 || in_line(i1, 0) == 0)) && (pos_caballo_abajo_abajo[i] <= 63 && pos_caballo_abajo_abajo[i] >= 0) && (pos_caballo_abajo_abajo[i] == i2))
      ) {
    mem[i2] = 0x05 | (c2 << 4);
    mem[i1] = 0;
    good = 1;
  }
      }
    }
  }
  if (good == 0) {
    *out = ERROR;
  }    
}

void verify_bishop(char *out, char in1, char in2) {
  /****************************************************************************

    ABSTRACT: Verifica movimiento del alfil.
    DESCRIPTION: La pieza en in1 es un alfil, así que hay que
    verificar que el movimiento hacia in2 sea válido.
    Notar la redundancia en la mayoría de los vectores de posiciones
    válidas: no dañan a nadie y ayudan a simplificar bastante la
    verificación.

   *****************************************************************************/
  // Indices
  int i1 = (int)in1;
  int i2 = (int)in2;

  // Color
  char c1 = (mem[i1] & 0x10)>>4;
  char c2 = 0;
  
  // Tipo
  char t2 = (mem[i2] & 0x0e)>>1;

  if (t2 != 0)
    c2 = (mem[i2] & 0x10)>>4;

  char good = 0;
  int i;

  if (((*out & 4) >> 2) == 0) { // si no hay error
    int pos_alfil_diag_derecha_abajo[9] = {i1+7, i1+14, i1+21, i1+28, i1+35, i1+42, i1+49, i1+56, i1+63};
    int pos_alfil_diag_izquierda_arriba[9] = {i1-7, i1-14, i1-21, i1-28, i1-35, i1-42, i1-49, i1-56, i1-63};
    int pos_alfil_diag_derecha_arriba[9] = {i1+9, i1+18, i1+27, i1+36, i1+45, i1+54, i1+63, i1+63, i1+63};
    int pos_alfil_diag_izquierda_abajo[9] = {i1-9, i1-18, i1-27, i1-36, i1-45, i1-54, i1-63, i1-63, i1-63};

    char no_arriba_derecha = 0;
    char no_arriba_izquierda = 0;
    char no_abajo_derecha = 0;
    char no_abajo_izquierda = 0;

    if ((t2 == 0) || (t2 != 0 && c2 != c1)) { // si la pieza de destino no es del mismo color o no hay pieza
      for (i=0; i<9; i++) {

  // Marcar si se llega a los limites del tablero para no proseguir
  if (i1 > 64 || in_line(pos_alfil_diag_derecha_arriba[i], 0) == 1) no_arriba_derecha = 1;
  if (i1 > 64 || in_line(pos_alfil_diag_derecha_abajo[i], 7) == 1) no_abajo_derecha = 1;
  if (i1 < 0 || in_line(pos_alfil_diag_izquierda_arriba[i], 0) == 1) no_arriba_izquierda = 1;
  if (i1 < 0 || in_line(pos_alfil_diag_izquierda_abajo[i], 7) == 1) no_abajo_izquierda = 1;
  
  // Marcar si hay una pieza entre origen y destino para no proseguir en esa direccion
  if ((i2 != pos_alfil_diag_derecha_abajo[i]) && (((mem[pos_alfil_diag_derecha_abajo[i]] & 0x0e) >> 1) != 0)) no_abajo_derecha = 1;
  if ((i2 != pos_alfil_diag_izquierda_abajo[i]) && (((mem[pos_alfil_diag_izquierda_abajo[i]] & 0x0e) >> 1) != 0)) no_abajo_izquierda = 1;
  if ((i2 != pos_alfil_diag_derecha_arriba[i]) && (((mem[pos_alfil_diag_derecha_arriba[i]] & 0x0e) >> 1) != 0)) no_arriba_derecha = 1;
  if ((i2 != pos_alfil_diag_izquierda_arriba[i]) && (((mem[pos_alfil_diag_izquierda_arriba[i]] & 0x0e) >> 1) != 0)) no_arriba_izquierda = 1;

  if (((pos_alfil_diag_derecha_abajo[i] == i2) && !no_abajo_derecha) // si la pieza esta en diagonal hacia la derecha abajo
      || ((pos_alfil_diag_derecha_arriba[i] == i2) && !no_arriba_derecha) // si la pieza esta en diagonal hacia la derecha arriba
      || ((pos_alfil_diag_izquierda_abajo[i] == i2) && !no_abajo_izquierda) // si la pieza esta en diagonal hacia la izquierda abajo
      || ((pos_alfil_diag_izquierda_arriba[i] == i2) && !no_arriba_izquierda) // si la pieza esta en diagonal hacia la izquierda arriba
      ) {     
    mem[i2] = 0x07 | (c2 << 4);
    mem[i1] = 0;
    good = 1;
  }
      }
    }
  }
  if (good == 0) {
    *out = ERROR;
  }    
}

void verify_rook(char *out, char in1, char in2) {
  /****************************************************************************

    ABSTRACT: Verifica movimiento de la torre.
    DESCRIPTION: La pieza en in1 es una torre, así que hay que
    verificar que el movimiento hacia in2 sea válido.

   *****************************************************************************/
  // Indices
  int i1 = (int)in1;
  int i2 = (int)in2;

  // Color
  char c1 = (mem[i1] & 0x10)>>4;
  char c2 = 0;
  
  // Tipo
  char t2 = (mem[i2] & 0x0e)>>1;

  if (t2 != 0)
    c2 = (mem[i2] & 0x10)>>4;

  char good = 0;

  int i;

  if (((*out & 4) >> 2) == 0) { // si no hay error
    int pos_torre_arriba[7] = {i1+1, i1+2, i1+3, i1+4, i1+5, i1+6, i1+7};
    int pos_torre_abajo[7] = {i1-1, i1-2, i1-3, i1-4, i1-5, i1-6, i1-7};
    int pos_torre_derecha[7] = {i1+8, i1+16, i1+24, i1+32, i1+40, i1+48, i1+56};
    int pos_torre_izquierda[7] = {i1-8, i1-16, i1-24, i1-32, i1-40, i1-48, i1-56};

    char no_arriba = 0;
    char no_abajo = 0;
    char no_derecha = 0;
    char no_izquierda = 0;

    if ((t2 == 0) || (c2 != c1)) { // pos 2 no tiene tipo o los colores son distintos

      for (i=0; i<7; i++) {

	// Marcar si se llega a los limites del tablero para no proseguir
	if (in_line(pos_torre_arriba[i], 0) == 1) no_arriba = 1;
	if (in_line(pos_torre_abajo[i], 7) == 1) no_abajo = 1;
	if (pos_torre_derecha[i] > 64) no_derecha = 1;
	if (pos_torre_izquierda[i] < 0) no_izquierda = 1;
	
	if ((i2 != pos_torre_arriba[i]) && (((mem[pos_torre_arriba[i]] & 0x0e) >> 1) != 0)) no_arriba = 1;
	if ((i2 != pos_torre_abajo[i]) && (((mem[pos_torre_abajo[i]] & 0x0e) >> 1) != 0)) no_abajo = 1;
	if ((i2 != pos_torre_derecha[i]) && (((mem[pos_torre_derecha[i]] & 0x0e) >> 1) != 0)) no_derecha = 1;
	if ((i2 != pos_torre_izquierda[i]) && (((mem[pos_torre_izquierda[i]] & 0x0e) >> 1) != 0)) no_izquierda = 1;
	
	if (((pos_torre_arriba[i] == i2) && !no_arriba) // si la pieza esta arriba
	    || ((pos_torre_abajo[i] == i2) && !no_abajo) // si la pieza esta abajo
	    || ((pos_torre_derecha[i] == i2) && !no_derecha) // si la pieza esta a la derecha
	    || ((pos_torre_izquierda[i] == i2) && !no_izquierda) // si la pieza esta a la izquierda
	    ) {
	  mem[i2] = 0x09 | (c1 << 4);
	  mem[i1] = 0;
	  good = 1;
	}
      }
    }
  }

  if (good == 0) {
    *out = ERROR;
  }
  
}

void verify_queen(char *out, char in1, char in2) {
  /****************************************************************************

    ABSTRACT: Verifica movimiento de la dama.
    DESCRIPTION: La pieza en in1 es una dama, así que hay que
    verificar que el movimiento hacia in2 sea válido.
    Notar la redundancia en la mayoría de los vectores de posiciones
    válidas: no dañan a nadie y ayudan a simplificar bastante la
    verificación.

   *****************************************************************************/
  // Indices
  int i1 = (int)in1;
  int i2 = (int)in2;

  // Color
  char c1 = (mem[i1] & 0x10)>>4;
  char c2;
  
  // Tipo
  char t2 = (mem[i2] & 0x0e)>>1;

  if (t2 != 0)
    c2 = (mem[i2] & 0x10)>>4;
  else
    c2 = 0;

  char good = 0;

  int i;

  if (((*out & 4) >> 2) == 0) { // si no hay error
    int pos_dama_arriba[9] = {i1+1, i1+2, i1+3, i1+4, i1+5, i1+6, i1+7, i1+7, i1+7};
    int pos_dama_abajo[9] = {i1-1, i1-2, i1-3, i1-4, i1-5, i1-6, i1-7, i1-7, i1-7};
    int pos_dama_derecha[9] = {i1+8, i1+16, i1+24, i1+32, i1+40, i1+48, i1+56, i1+56, i1+56};
    int pos_dama_izquierda[9] = {i1-8, i1-16, i1-24, i1-32, i1-40, i1-48, i1-56, i1-56, i1-56};
    int pos_dama_diag_derecha_abajo[9] = {i1+7, i1+14, i1+21, i1+28, i1+35, i1+42, i1+49, i1+56, i1+63};
    int pos_dama_diag_izquierda_arriba[9] = {i1-7, i1-14, i1-21, i1-28, i1-35, i1-42, i1-49, i1-56, i1-63};
    int pos_dama_diag_derecha_arriba[9] = {i1+9, i1+18, i1+27, i1+36, i1+45, i1+54, i1+63, i1+63, i1+63};
    int pos_dama_diag_izquierda_abajo[9] = {i1-9, i1-18, i1-27, i1-36, i1-45, i1-54, i1-63, i1-63, i1-63};

    char no_arriba = 0;
    char no_abajo = 0;
    char no_derecha = 0;
    char no_izquierda = 0;
    char no_arriba_derecha = 0;
    char no_arriba_izquierda = 0;
    char no_abajo_derecha = 0;
    char no_abajo_izquierda = 0;

    if ((t2 == 0) || (t2 != 0 && c2 != c1)) { // si la pieza de destino no es del mismo color o no hay pieza
      for (i=0; i<9; i++) {

	// Marcar si se llega a los limites del tablero para no proseguir
	if (in_line(pos_dama_arriba[i], 0) == 1) no_arriba = 1;
	if (in_line(pos_dama_abajo[i], 7) == 1) no_abajo = 1;
	if (pos_dama_derecha[i] > 64) no_derecha = 1;
	if (pos_dama_izquierda[i] < 0) no_izquierda = 1;
	if (i1 > 64 || in_line(pos_dama_diag_derecha_arriba[i], 0) == 1) no_arriba_derecha = 1;
	if (i1 > 64 || in_line(pos_dama_diag_derecha_abajo[i], 7) == 1) no_abajo_derecha = 1;
	if (i1 < 0 || in_line(pos_dama_diag_izquierda_arriba[i], 0) == 1) no_arriba_izquierda = 1;
	if (i1 < 0 || in_line(pos_dama_diag_izquierda_abajo[i], 7) == 1) no_abajo_izquierda = 1;
	
	// Marcar si hay una pieza entre origen y destino para no proseguir en esa direccion
	if ((i2 != pos_dama_arriba[i]) && (((mem[pos_dama_arriba[i]] & 0x0e) >> 1) != 0)) no_arriba = 1;
	if ((i2 != pos_dama_abajo[i]) && (((mem[pos_dama_abajo[i]] & 0x0e) >> 1) != 0)) no_abajo = 1;
	if ((i2 != pos_dama_derecha[i]) && (((mem[pos_dama_derecha[i]] & 0x0e) >> 1) != 0)) no_derecha = 1;
	if ((i2 != pos_dama_izquierda[i]) && (((mem[pos_dama_izquierda[i]] & 0x0e) >> 1) != 0)) no_izquierda = 1;
	if ((i2 != pos_dama_diag_derecha_abajo[i]) && (((mem[pos_dama_diag_derecha_abajo[i]] & 0x0e) >> 1) != 0)) no_abajo_derecha = 1;
	if ((i2 != pos_dama_diag_izquierda_abajo[i]) && (((mem[pos_dama_diag_izquierda_abajo[i]] & 0x0e) >> 1) != 0)) no_abajo_izquierda = 1;
	if ((i2 != pos_dama_diag_derecha_arriba[i]) && (((mem[pos_dama_diag_derecha_arriba[i]] & 0x0e) >> 1) != 0)) no_arriba_derecha = 1;
	if ((i2 != pos_dama_diag_izquierda_arriba[i]) && (((mem[pos_dama_diag_izquierda_arriba[i]] & 0x0e) >> 1) != 0)) no_arriba_izquierda = 1;
	
	if (((pos_dama_arriba[i] == i2) && !no_arriba) // si la pieza esta arriba
	    || ((pos_dama_abajo[i] == i2) && !no_abajo) // si la pieza esta abajo
	    || ((pos_dama_derecha[i] == i2) && !no_derecha) // si la pieza esta a la derecha
	    || ((pos_dama_izquierda[i] == i2) && !no_izquierda) // si la pieza esta a la izquierda
	    || ((pos_dama_diag_derecha_abajo[i] == i2) && !no_abajo_derecha) // si la pieza esta en diagonal hacia la derecha abajo
	    || ((pos_dama_diag_derecha_arriba[i] == i2) && !no_arriba_derecha) // si la pieza esta en diagonal hacia la derecha arriba
	    || ((pos_dama_diag_izquierda_abajo[i] == i2) && !no_abajo_izquierda) // si la pieza esta en diagonal hacia la izquierda abajo
	    || ((pos_dama_diag_izquierda_arriba[i] == i2) && !no_arriba_izquierda) // si la pieza esta en diagonal hacia la izquierda arriba
	    ) {     
	  mem[i2] = 0x0b | (c1 << 4);
	  mem[i1] = 0;
	  good = 1;
	}
      }
    }
  }
  if (good == 0) {
    *out = ERROR;
  }    
}

void verify_castling(char*out, char in1, char in2) {
  /****************************************************************************

    ABSTRACT: Verifica enroque del rey.
    DESCRIPTION: El enroque puede ser del rey blanco o negro, hacia la
    A (enroque largo) o hacia la H (enroque corto).

   *****************************************************************************/
  // Indices
  int i1 = (int)in1;
  int i2 = (int)in2;

  int good = 0;

  if (((*out & 4) >> 2) == 0) {
    if ((mem[i1] & 0x01) == 0) { // rey no ha sido movido
      if (i1 == 32) { // rey blanco: 32
  if (i2 == 16) { // enroque largo
    if (mem[0] == 0x18) { // torre no ha sido movida
      if (mem[8] == 0 && mem[16] == 0 && mem[24] == 0) {
        mem[i1] = 0;
        mem[i2] = 0x1d;
        mem[24] = 0x19;
        mem[0] = 0;
        good = 1;
      }
    }
  } else if (i2 == 48) { // enroque corto
    if (mem[56] == 0x18) { // torre no ha sido movida
      if (mem[40] == 0 && mem[48] == 0) {
        mem[i1] = 0;
        mem[i2] = 0x1d;
        mem[40] = 0x19;
        mem[56] = 0;
        good = 1;
      }
    }
  }
      } else if (i1 == 39) { // rey negro: 39
  if (i2 == 25) { // enroque largo
    if (mem[7] == 0x08) { // torre no ha sido movida
      if (mem[15] == 0 && mem[23] == 0 && mem[31] == 0) {
        mem[i1] = 0;
        mem[i2] = 0x0d;
        mem[31] = 0x09;
        mem[7] = 0;
        good = 1;
      }
    }
  } else if (i2 == 57) { // enroque corto
    if (mem[63] == 0x08) { // torre no ha sido movida
      if (mem[47] == 0 && mem[55] == 0) {
        mem[i1] = 0;
        mem[i2] = 0x0d;
        mem[47] = 0x09;
        mem[63] = 0;
        good = 1;
      }     
    }
  }
      }
    }
  }
  if (good == 0) {
    *out = ERROR;
  }
}    

void verify_king(char*out, char in1, char in2) {
  /****************************************************************************

    ABSTRACT: Verifica movimiento de un rey que no está en jaque.

   *****************************************************************************/
  // Indices
  int i1 = (int)in1;
  int i2 = (int)in2;

  // Color
  char c1 = (mem[i1] & 0x10)>>4;
  char c2;
  
  // Tipo
  char t2 = (mem[i2] & 0x0e)>>1;

  char good = 0;

  if (t2 != 0)
    c2 = (mem[i2] & 0x10)>>4;
  else
    c2 = 0;

  if (((*out & 4) >> 2) == 0) { // si no hay error
    if ((t2 == 0) || (t2 != 0 && c2 != c1)) { // y el destino es vacio o una pieza del contrincante
      // si el movimiento es válido para el rey
      if (
    // verificar que no sea el limite o las esquinas inferiores
    ((i2 == i1 - 1) && (in_line(i1, 0) == 0)) || ((i2 == i1 - 9) && (in_line(i1, 0) == 0) && (i2 > 0)) || ((i2 == i1 + 7) && (in_line(i1, 0) == 0) && (i2 < 64)) 
    // verificar que no sea el limite o las esquinas superiores
    || ((i2 == i1 + 1) && (in_line(i1, 7) == 0)) || ((i2 == i1 + 9) && (in_line(i1, 7) == 0) && (i2 < 64)) || ((in_line(i1, 7) == 0))
          || ((i2 == i1 + 8)  && (i2 < 64)) // verificar que no sea el limite derecho
    || ((i2 == i1 - 8) && (i2 > 0)) // verificar que no sea el limite izquierdo
    ) {
  char aux1 = mem[i1];
  char aux2 = mem[i2];
  mem[i2] = 0x0d | (c1 << 4);
  mem[i1] = 0;
  good = 1;
  is_check(out);
  if (((mem[i2] & 0x0e) >> 1) == 0x07) {
    mem[i2] = aux2;
    mem[i1] = aux1;
    good = 0;
  }
      } else {
  verify_castling(out, in1, in2);
      }
    }
  }
  if (good == 0 || (*out >> 2) == 1)
    *out = ERROR;
}

void verify_check(char *out, char in1, char in2) {
  /****************************************************************************

    ABSTRACT: Verifica movimiento de un rey en jaque.

   *****************************************************************************/

  // Indices
  int i1 = (int)in1;
  int i2 = (int)in2;

  // Color
  char c1 = (mem[i1] & 0x10)>>4;
  char c2;
  
  // Tipo
  char t2 = (mem[i2] & 0x0e)>>1;

  char good = 0;

  if (t2 != 0)
    c2 = (mem[i2] & 0x10)>>4;
  else
    c2 = 0;

  if (((*out & 4) >> 2) == 0) { // no hay error
    if ((t2 == 0) || (t2 != 0 && c2 != c1)) { // destino no tiene pieza o bien la pieza que hay es del contrincante
      if (i2 == i1 + 1 || i2 == i1 - 1 || i2 == i1 + 8 || i2 == i1 - 8 || i2 == i1 + 9 || i2 == i1 + 7 || i2 == i1 - 9 || i2 == i1 - 7) { // movimiento válido para un rey
    char aux1 = mem[i1];
    char aux2 = mem[i2];
    mem[i2] = 0x0d | (c1 << 4); // destino es un rey
    mem[i1] = 0; // origen no es nada
    good = 1;
    is_check(out); // verificar que el nuevo rey no este en jaque
    if (((mem[i2] & 0x0e) >> 1) == 0x07) { // si lo está
      mem[i2] = aux2; // abortar movimiento
      mem[i1] = aux1; // abortar movimiento
      good = 0;
    }
      }
    }
  }
  if (good == 0)
    *out = ERROR;
}

void check_player(char *out, char in) {
  /****************************************************************************

    ABSTRACT: Comprueba que el color de la pieza seleccionada como
    origen coincida con el color del jugador del turno actual.

   *****************************************************************************/
  int i = (int)in;
  char c = mem[i] & 0x10;
  c>>=4;
  
  // Si coinciden el jugador anterior y el color de la pieza de entrada, error
  if (c != curr_player) {    
    *out = ERROR;
  }
}

void check_game(char *out) {
  /****************************************************************************

    ABSTRACT: Comprueba que no haya un jaque mate.

   *****************************************************************************/
  if (((*out & 4) >> 2) == 0 && ((*out & 8) >> 3) == 0) {
    is_check_mate(out);
  }
}


void promote(char *out, char in1, char in2) {
  
  int i1 = (int)in1;
  int i2 = (int)in2;

  int t1 = i1 << 1; // Tipo a convertir
  char c = (mem[i2] & 0x10) >> 4;

  if (c == 1){
    mem[i2] = mem[i2] & 0x10; // borra tipo peon
    mem[i2] = mem[i2] | t1;    // agrega nuevo tipo
  }
  else{
    mem[i2] = mem[i2] & 0x00;
    mem[i2] = mem[i2] | t1;
  }

  *out = 0;
  is_check(out);

}

void print_eog(char *out) {
  /****************************************************************************

    ABSTRACT: De acuerdo a la salida del circuito, se imprime el mensaje 
     correspondiente.

   *****************************************************************************/
  switch(*out) 
    {
    case WWINS:
      printf("Ganador: blanco\n");
      break;
    case BWINS:
      printf("Ganador: negro\n");
      break;
    case ERROR:
      printf("Error!\n");
      break;
    default:
      break;
    }
  if (*out == ERROR) {
    *out = 0;
  }
}

void chess_verify(char *out, char in1, char in2, int reset) {
  /****************************************************************************

    ABSTRACT: Restablece tablero si hay reset. Comprueba que el origen coincida
     en color con el color del jugador. Extrae el tipo de la pieza de entrada y 
     verifica el movimiento de acuerdo a su tipo. En caso de no ser una pieza la
     entrada, tira error.

   *****************************************************************************/
  check_board(reset); // Restablecer tablero si es el caso
  check_player(out, in1); // Comprueba que el color de la pieza de
        // origen y del jugador actual coincidan.
  
  int i = (int)in1;
  int type_orig = (mem[i] & 0x0e) >> 1;
  switch(type_orig) // verificamos y ejecutamos jugada de acuerdo a
        // cual es el origen
    {
    case 01:
      verify_pawn(out, in1, in2);
      break;
    case 02:
      verify_knight(out, in1, in2);
      break;
    case 03:
      verify_bishop(out, in1, in2);
      break;
    case 04:
      verify_rook(out, in1, in2);
      break;
    case 05:
      verify_queen(out, in1, in2);
      break;
    case 06:
      verify_king(out, in1, in2);
      break;
    case 07:
      verify_check(out, in1, in2);
      break;
    default:
      *out = ERROR; // si el origen es un espacio vacio, es un error
      break;
    }
  
  check_game(out); // comprueba si el juego ya se ha acabado

  if (*out != ERROR)
    curr_player = !(curr_player);

  print_eog(out); // imprime error o fin de juego, de ser el caso
}

int process_input(char *c, char *str) {
  /****************************************************************************

    ABSTRACT: Toma string de entrada y lo convierte en un numero unico
     de 6 bits con la posicion en el tablero.

    DESCRIPTION: La posicion de entrada tiene la forma [A-H][1-8] en ASCII y la
     funcion codifica esta entrada en un numero de 6 bits donde los primeros 3 
     bits son la letra (columna) y los ultimos 3 son el numero (fila) en el 
     tablero. La codificacion es como sigue:
     In     Out
     A <--> 00
     B <--> 01
     C <--> 02
     D <--> 03
     E <--> 04
     F <--> 05
     G <--> 06
     H <--> 07

     1 <--> 00
     2 <--> 01
     3 <--> 02
     4 <--> 03
     5 <--> 04
     6 <--> 05
     7 <--> 06
     8 <--> 07

    Ejemplos: A1 <--> 000. D7 <--> 036. F3 <--> 052.

    Nota: anteponer 0 es indicarle al compilador de C que la base del byte es 8 
     (octal).
     
  *****************************************************************************/
  if (strlen(str) != 2) // Si el tamaño no es el esperado (2)
    return 0; // Salir
  if (str[0] < 'A' || str[0] > 'H') // Rango de letras no corresponde
    return 0;
  if (str[1] < '1' || str[1] > '8') // Rango de numeros no corresponde
    return 0;
  
  char letra = str[0]-'A'; // Ajuste de letras (ASCII)
  char numero = str[1]-'1'; // Ajuste de numeros

  letra<<=3; // Prepara bits de letras para concatenacion
  *c = (letra | numero); // Concatenacion
  return 1; // Retorna true
}

int process_input_special(char *c, char *str) {
  /****************************************************************************

    ABSTRACT: Toma string de entrada y lo convierte en un numero unico
     de 3 bits indicando la pieza a promocionar

    DESCRIPTION: El tipo de la entrada puede tomar valores entre [2-5] en ASCII
     y la funcion codifica esta entrada en un numero de 3 bits que representa
     el tipo de la pieza a cambiar del peon. La codificacion es como sigue:

     In     Out
     2 <--> 010  Caballo
     3 <--> 011  Alfil
     4 <--> 100  Torre
     5 <--> 101  Reina

    Nota: anteponer 0 es indicarle al compilador de C que la base del byte es 8 
     (octal).
     
  *****************************************************************************/
  if (strlen(str) != 1) // Si el tamaño no es el esperado (1)
    return 0; // Salir
  if (str[0] < '2' || str[0] > '5') // Rango de numeros no corresponde
    return 0;
  
  char numero = str[0]-'0'; // Ajuste de numeros

  *c = numero;
  return 1; // Retorna true
}
